# README #

### What is this repository for? ###

A web design portfolio piece demonstrating a single page design for an adventure website. The simple design includes a parallax background effect.

### Can I see the project working? ###

* Clone this repository and Open in File Explorer
* Right-Click 'index.html'
* Click 'Open in browser'

### Key Skills Demonstrated ###

* HTML
* CSS